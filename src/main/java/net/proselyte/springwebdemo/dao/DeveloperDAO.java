package net.proselyte.springwebdemo.dao;

import net.proselyte.springwebdemo.model.Developer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeveloperDAO extends JpaRepository<Developer, Long> {
}