package net.proselyte.springwebdemo.service;

import net.proselyte.springwebdemo.dao.DeveloperDAO;
import net.proselyte.springwebdemo.model.Developer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DeveloperServiceImpl implements DeveloperService {

    @Autowired
    private DeveloperDAO developerDAO;

    public List<Developer> findAll() {
        return developerDAO.findAll();
    }
}
