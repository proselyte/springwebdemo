package net.proselyte.springwebdemo.service;

import net.proselyte.springwebdemo.model.Developer;

import java.util.List;

public interface DeveloperService {

    List<Developer> findAll();
}
