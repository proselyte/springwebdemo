package net.proselyte.springwebdemo.controller;

import net.proselyte.springwebdemo.service.DeveloperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class DeveloperController {

    @Autowired
    private DeveloperService developerService;

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String test(){
        System.out.println("LOGGER: test page is called...");
        return "test";
    }

    @RequestMapping(value = "/developer/list", method = RequestMethod.GET)
    public String finaAllDevelopers(){
        System.out.println(developerService.findAll());
        return "developers";
    }
}
